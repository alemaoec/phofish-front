import Vue from 'vue'
import VueAxios from 'vue-axios'
import VueAuthenticate from 'vue-authenticate'
import axios from 'axios';

Vue.use(VueAxios, axios)
Vue.use(VueAuthenticate, {
  baseUrl: 'https://api.phofish.com/',

  providers: {
    facebook: {
      clientId: '733728573355175',
      redirectUri: 'http://localhost:8080/auth/callback' // Your client app URL
    }
  }
})
